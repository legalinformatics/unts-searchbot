# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pyvirtualdisplay import Display
import time, re, math
from datetime import date, datetime
import pandas as pd

# set working directory
wd = './examples/'

def parse_table(rows, period):
    """ Extract treaty records from UNTS search results page and return as list of dicts """
    colnb = dict(regNb=2, regDate=3, UNTStreatyType=4, adoptDate=5, UNTSvolNb=6)
    result = []
    treaty = {}
    for i in range(len(rows)):
       treaty['title'] = rows[i].find_element('xpath', './td[1]/a').get_attribute('title')
       treaty['UNTSurl'] = rows[i].find_element('xpath', './td[1]/a').get_attribute('href')
       treaty['lastRetrieved'] = str(datetime.utcnow().isoformat().split(".")[0]+'Z')
       treaty['adoptPeriod'] = str(period) 
       for k,v in colnb.items():
          treaty[k] = rows[i].find_element('xpath', f'./td[{v}]').text
       result.append(treaty.copy())
    return result

starttime = datetime.now()
# set start and end month of desired time period
prng = pd.period_range('2022-07', '2022-12', freq='M')
records = pd.Series(index=prng, dtype=float)
treatylist = []
idctl = '//*[@id="ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolderInnerPage_'
treatyxpath = idctl + 'drpSearchObj"]/option[@value="ts_treaty"]'
adoptxpath = idctl + 'drpAttribute"]/option[@value="conclusion_info_id"]'
addxpath = idctl + 'btnAdd"]'
submitxpath = idctl + 'btnSubmit"]'
rcxpath = '//*[@class="RecordCount"]'
rowsxpath = idctl + 'dgTreaty"]//tr[@align="left"]'
nextxpath = '//input[@src="../Images/Paging/btn_next.jpg"]'

# loop through months (due to max results limit per search)
for period in prng:
    display = Display(visible=0, size=(1200, 1200)).start()
    s = Service('/usr/bin/chromedriver')
    driver = webdriver.Chrome(service=s)
    wait = WebDriverWait(driver, 300)
    driver.get('https://treaties.un.org/Pages/AdvanceSearch.aspx?tab=UNTS&clang=_en')
    start = period.to_timestamp('D', how='s').strftime('%d/%m/%Y')
    end = period.to_timestamp('D', how='e').strftime('%d/%m/%Y')
    wait.until(EC.element_to_be_clickable((By.XPATH, treatyxpath))).click()
    wait.until(EC.element_to_be_clickable((By.XPATH, adoptxpath))).click()
    wait.until(EC.element_to_be_clickable((By.XPATH, idctl+'txtFrom"]'))).send_keys(start)
    wait.until(EC.element_to_be_clickable((By.XPATH, idctl+'txtTo"]'))).click()
    id = 'ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolderInnerPage_txtTo'
    # .send_keys() doesn't work due to autofill on focus
    driver.execute_script(f"document.getElementById('{id}').value='{end}'")
    # save screenshot of form with From-To dates filled in
    driver.save_screenshot(wd+'FormFilled.png')
    addButton = wait.until(EC.element_to_be_clickable((By.XPATH, addxpath)))
    addButton.click()
    wait.until(EC.staleness_of(addButton))
    # save screenshot of submitted form (for debugging)
    driver.save_screenshot(wd+'FormSubmitted.png')
    submitButton = wait.until(EC.element_to_be_clickable((By.XPATH, submitxpath)))
    # submitButton.click() returns 'element is not clickable at point' error
    driver.execute_script('arguments[0].click();', submitButton)
    recordCount = wait.until(EC.visibility_of_element_located((By.XPATH, rcxpath)))
    records[period] = int(re.search(r"Record Count : (\d+)\s*", recordCount.text).group(1))
    # save count of treaty records per month
    records.to_frame(name='recordCount').to_csv(wd+'recordcount.csv', encoding='utf-8', date_format='%Y-%m', index_label='month')
    nb = math.ceil(records[period]/10)
    for n in range(1,nb+1):
         rows =  wait.until(EC.presence_of_all_elements_located((By.XPATH, rowsxpath)))
         treatylist.extend(parse_table(rows, period))
         if n != nb:
             next = wait.until(EC.visibility_of_element_located((By.XPATH, nextxpath)))
             next.click()
             wait.until(EC.staleness_of(rows[1]))

    treatydf = pd.DataFrame(treatylist)
    treatydf.to_csv(wd+'treatydf.csv', encoding='utf-8', index=False)
    driver.quit()
    display.stop()
    time.sleep(2)

elapsed = str(datetime.now() - starttime).split(".")[0]
print(f'Done! Treaty records fetched: {len(treatydf)}. Time elapsed: {elapsed}')
