# UNTS search bot

This project develops an automatic United Nations treaty search bot for efficient and reproducible legal research.

## About the data
The United Nations Treaty Series [(UNTS)](https://treaties.un.org/Pages/Content.aspx?path=DB/UNTS/pageIntro_en.xml) is by far the largest and most authoritative collection of international agreements of the UN era. The canonical way of referring to a treaty in publications also includes the UNTS volume and page number on which it appears. Under Article 102 of the [UN Charter](https://www.un.org/en/about-us/un-charter/full-text) all international agreements entered into by its member states must be registered with the UN Secretariat and be published by it.

UNTS is not to be confused with the collection of Multilateral Treaties Deposited with the Secretary-General [(MTDSG)](https://treaties.un.org/Pages/Content.aspx?path=DB/MTDSGStatus/pageIntro_en.xml) which is a small subset of UNTS, but sometimes contains additional notes and links to documents because the UN Secretary-General is depositary of these treaties. The Github repo [untreaties](https://github.com/zmjones/untreaties) has a set of scripts to gather MTDSG data (not the whole UN treaty collection as the developer seemed to think). Depositary and registrar roles are different. 

## Related projects
This little bot is part of a pipeline, and is deliberately focused only on gathering UNTS database/index metadata, saving treaty page urls for subsequent use by [UNTS-crawler](https://gitlab.com/legalinformatics/unts-crawler). The reason for separating these two parts of the data gathering process is that historical index data is hardly ever updated whereas individual treaty pages should be updated whenever new parties join a multilateral agreement. Also, treaty/document page urls stay the same over time and these pages can be very efficiently scraped with [Scrapy](https://scrapy.org/) whereas using the search engine requires a browser automation tool ([Selenium](https://www.selenium.dev/) in this case).

As for downstream uses of the data:
- [PILO](https://gitlab.com/legalinformatics/pilo) is a Public International Law Ontology in OWL2 format developed for the purpose of data integration
- [UNTS-treatyrecords-IE](https://gitlab.com/legalinformatics/unts-treatyrecords-ie) is a program for annotating UNTS treaty records harvested with [UNTS-crawler](https://gitlab.com/legalinformatics/unts-crawler) and populating PILO with relevant data (not including texts from UNTS volumes in PDF)
- [Treaty-references](https://gitlab.com/martinakunz/treaty-references) uses the automatically retrieved UNTS data to generate a bibliographic database and other files for use in publications
- [Treaty-participation-maps](https://gitlab.com/legalinformatics/treaty-participation-maps) uses UNTS participation data retrieved from PILO for interactive treaty participation maps

More background and details will be described at https://martinakunz.gitlab.io/treaty-analytics/

## Software requirements
- Operating system: I use Debian Linux and have not tested the code on other systems, but it should be cross-platform (Windows users would need to modify file paths in the script from `/` to `\` I presume)
- Python: I use Python 3.9 but other versions >3.5 may work as well (see `requirements.txt` file)
- Python packages: See the `requirements.txt` file or `Pipfile.lock` for the exact versions I used in the last run, and `Pipfile` for minimal dependency specifications (only Selenium, Pandas and PyVirtualDisplay) -- I recommend using [Pipenv](https://pipenv.pypa.io/) for Python dependency management when reproducible environments matter
  - `pipenv install --ignore-pipfile` installs packages based on `Pipfile.lock` which enables a deterministic build
  - `pipenv check` conducts a safety assessment and lists known vulnerabilities of the packages installed (none at the time of the last run)  
- Selenium webdriver: Selenium requires a [webdriver](https://www.selenium.dev/documentation/webdriver/getting_started/); I have used the `chromium-driver` Debian package for Chromium versions 106 and 113, but others should work too (this may require some code edits)

## Reproducibility
On a new Debian or Ubuntu computer, I would first install dependencies and reproduce the bot with:

```
sudo apt update && sudo apt install git python3.9 python3-pip chromium-driver=113.*
pip3 install --user pipenv==2023.10.3
git clone https://gitlab.com/legalinformatics/unts-searchbot.git
cd unts-searchbot
pipenv install --ignore-pipfile 
pipenv shell
python3 unts_searchbot.py
```
The source data is likely to have changed in the meantime, so the output files will not be the same, but it should at least be similar. 

## How it works
The bot fills in the search form in the database with predefined parameters, submits the form and then scrapes information contained in the search results pages until it reaches the last page. Currently it's set to request all treaties (including amending agreements) which have been adopted in a given month, looping through a defined time range. Longer intervals could be entered into the UNTS [search form](https://treaties.un.org/Pages/AdvanceSearch.aspx?tab=UNTS&clang=_en_) but the maximum number of results returned is 500, thus some treaties might be missed when choosing a longer time span. So far the month with the largest number of treaty records was June 1976 with 188 records (some of which could be duplicates, that gets cleaned later).

***Important:*** Open the python script and edit the relevant parameters before you run it! Especially:
- `wd`: working directory where the data will be saved
- `prng`: period range of search (first and last month of the search)
- `driver`: location of the webdriver on your system (to be installed first)
- `recordcount.csv`: file name of the monthly record count (if you want it)
- `treatydf.csv`: file name of the retrieved treaty metadata

The webdriver saves screenshots after filling in the search form and before submitting to help with debugging. Here's an example of a submitted form:

![screenshot](examples/FormSubmitted.png "Form submitted")

You can also watch the bot do all the tedious data collection for you by turning on display visibility in the line `display = Display(visible=0, size=(1200, 1200)).start()` (edit it to make `visible=1`) and enjoy :)

## Responsible use
Please DO NOT unnecessarily overload the UN servers! Only run the script if and when you really need it.

I include the latest `recordcount.csv` and `treatydf.csv` in this repository in case this is enough for your purposes, though bear in mind that there is often a delay between adoption/entry into force and registration of a treaty, and thus the monthly counts of the last few years are not representative of the actual number of treaties adopted during that time.
